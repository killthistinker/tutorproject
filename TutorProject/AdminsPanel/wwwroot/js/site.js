﻿let isVisible = false;
let isVisible2 = false;
const divSub = document.getElementById('addSub');
const divSpec = document.getElementById('addSpec');


function trigger() {
    isVisible = !isVisible;
    divSub.style.display = isVisible ? "block" : "none";
};
function trigger2() {
    isVisible2 = !isVisible2;
    divSpec.style.display = isVisible2 ? "block" : "none";
}


document
    .getElementById('push')
    .addEventListener('click', trigger);

document
    .getElementById('push2')
    .addEventListener('click', trigger2);


$('#ajaxTestButton1').click(function () {
    let jsonData = {'Name': $("#specName").val(), 'Description': $("#specDesc").val(), 'SubjectName': $("#subjectName").val()}
    $.ajax({
        type: "POST",
        url: "/Entities/AddSpeciality",
        data: JSON.stringify(jsonData),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
});
