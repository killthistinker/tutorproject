﻿const specialises = $('#specialises')
const myModalEl = document.getElementById('exampleModal')
const email = $('#email-value').attr('value')
const alertMessage = $('#alertMessage')

function refuseTutor() {
    const refuseTutor = {message:'', email:''}
    alertMessage.empty();
    const refuseCommentary = $('#refuseCommentary').val()

    if (refuseCommentary.length === 0){
    alertMessage.append('<p class="text-danger">Заполните поле</p>')
    return
    }

    refuseTutor.email = email
    refuseTutor.message = refuseCommentary
    sendRefuse(refuseTutor);
    return
}



function sendRefuse (refuseTutor){
    $.ajax({
        url: "https://localhost:7279/TutorApplications/Refuse",
        method: "POST",
        data: { "model": refuseTutor },
        success: function (response) {
            if (response === 700) {
                document.location.href = 'https://localhost:7279/TutorApplications/GetAllApplications'
            }
        }
    })
}

myModalEl.addEventListener('show.bs.modal', event => {
    specialises.empty()
    let values = []
    $('input:checkbox:checked').each(function () {
        values.push($(this).attr('specialise'))
    })

    if (!InputValidation(values)) return

    $.ajax({
        url: `https://localhost:7279/Specializes/GetAll?values=${values}`,
        type: "GET",
        success: function (response) {
            let increment = 0
            response.speicalities.forEach(element => {
                specialises.append(`
              <div>
                 <div class="card text-bg-dark mb-3" style="width: 100%">
                    <div class="card-header">${element.name}</div>
                     <div class="card-body">
                         <h5 class="card-title">${element.subjectName}</h5>
                         <p class="card-text">${element.description}</p>
                         <input element="${element.name}" class="form-check-input" type="checkbox" id="inlineCheckbox-${increment}" value="option1">
                          <label class="form-check-label" for="inlineCheckbox-${increment}">Выбрать</label>
                      </div>
                     </div>
                  </div>`)
                increment++
            });
        }
    })
})

function InputValidation(element) {
    if (element.length === 0) {
        specialises.append('<p class="text-danger">Выберите предмет</p>')
        return false
    }
    return true
}

function createTutor() {
    var tutor = {checked: [],email:''}
    tutor.email = email
    $('input:checkbox:checked').each(function () {
        tutor.checked.push($(this).attr('element'))
    })
    if (tutor.checked.length === 0) {
        var btn = $('#createTutor')
        btn.popover('show')
        return;
    }
    $.ajax({
        url: "https://localhost:7279/TutorApplications/Add",
        method: "POST",
        data: { "model": tutor },
        success: function (response) {
            if (response === 700) {
                document.location.href = 'https://localhost:7279/TutorApplications/GetAllApplications'
            }
        }
    })
}