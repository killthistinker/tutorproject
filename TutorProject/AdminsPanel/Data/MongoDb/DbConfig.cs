﻿namespace AdminsPanel.Data.MogoDb
{
    public class DbConfig
    {
        public string ConnectionString { get; set; } = "mongodb://localhost:27017/EducationalEntities";
        public string DatabaseName { get; set; } = "EducationalEntities";
        public string SubjectCollection { get; set; } = "Subject";
        public string SpecialityCollection { get; set; } = "Speciality";
        public string SpecialityLevelCollection { get; set; } = "SpecialityLevel";
    }
}