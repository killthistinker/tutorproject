﻿using AdminsPanel.Entities;
using MongoDB.Driver;

namespace AdminsPanel.Data.MogoDb
{
    public class EntityContextSeed
    {
        public static void SeedData(IMongoCollection<Subject> subjects)
        {
            bool existSubject = subjects.Find(p => true).Any();
            if (!existSubject)
            {
                subjects.InsertMany(GetPreconfiguredList());
            }
        }

        public static void SeedDataTwo(IMongoCollection<Speciality> specialities)
        {
            bool existSpeciality = specialities.Find(p => true).Any();
            if (!existSpeciality)
            {
                specialities.InsertMany(GetPreconfiguredListSpeciality());
            }
        }

        private static IEnumerable<Subject> GetPreconfiguredList()
        {
            return new List<Subject>()
            {
                new()
                {
                    Description = "Обучение Английскому языку и грамоте",
                    Name = "Английский язык"
                },
                new()
                {
                    Description = "Уроки компьютерной грамотности",
                    Name = "Информатика"
                },
                new()
                {
                    Description = "Курсы направленые на обучению инструментам и колористики",
                    Name = "Дизайн"
                },
            };
        }
        private static IEnumerable<Speciality> GetPreconfiguredListSpeciality()
        {
            return new List<Speciality>()
            {
                new()
                {
                    Description = "Подготовка в экзамену IELTS для получение сертификата",
                    Name = "IELTS",
                    SubjectName = "Английский язык"
                },
                new()
                {
                    Description = "Подготовка в экзамену TOEFL для получение сертификата",
                    Name = "TOEFL",
                    SubjectName = "Английский язык"
                },
                new()
                {
                    Description = "Развитие разговорной речи на реальных примерах взаимодействия",
                    Name = "Разговорный",
                    SubjectName = "Английский язык"
                },
                new()
                {
                    Description = "Ознакомление с инструментарием программы",
                    Name = "PhotoShop Beginner",
                    SubjectName = "Дизайн"
                },
                new()
                {
                    Description = "Фундаментальные навыки верстки",
                    Name = "InDesign",
                    SubjectName = "Дизайн"
                },
            };
        }
    }
}