﻿using AdminsPanel.Entities;
using MongoDB.Driver;

namespace AdminsPanel.Data.MongoDb;

public interface IEntityContext
{
    IMongoCollection<Subject> Subjects { get; }
    IMongoCollection<Speciality> Specialities { get; }
    IMongoCollection<SpecialityLevel> SpecialitiesLevel { get; }
}