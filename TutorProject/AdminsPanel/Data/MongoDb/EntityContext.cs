﻿using AdminsPanel.Data.MogoDb;
using AdminsPanel.Data.MongoDb;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using AdminsPanel.Entities;

namespace AdminsPanel.Data;

public class EntityContext : IEntityContext
{
    public EntityContext(IOptions<DbConfig> options)
    {
        MongoClient client = new MongoClient(options.Value.ConnectionString);
        IMongoDatabase dataBase = client.GetDatabase(options.Value.DatabaseName);
        Subjects = dataBase.GetCollection<Subject>(options.Value.SubjectCollection);
        Specialities = dataBase.GetCollection<Speciality>(options.Value.SpecialityCollection);
        SpecialitiesLevel = dataBase.GetCollection<SpecialityLevel>(options.Value.SpecialityLevelCollection);
        EntityContextSeed.SeedData(Subjects);
        EntityContextSeed.SeedDataTwo(Specialities);
    }
    public IMongoCollection<Subject> Subjects { get; }
    public IMongoCollection<Speciality> Specialities { get; }
    public IMongoCollection<SpecialityLevel> SpecialitiesLevel { get; }
}
