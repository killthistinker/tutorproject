﻿using AdminsPanel.Entities;
using Microsoft.EntityFrameworkCore;

namespace AdminsPanel.Data.PostgresDb;

public class TutorDbContext : DbContext
{
    public DbSet<TutorApplication> Applications { get; set; }

    public TutorDbContext(DbContextOptions<TutorDbContext> options) : base(options)
    {
        
    }
}