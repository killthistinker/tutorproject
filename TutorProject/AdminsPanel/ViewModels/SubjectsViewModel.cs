﻿namespace TutorAdmin.ViewModels;

public class SubjectsViewModel
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string? Description { get; set; }
}