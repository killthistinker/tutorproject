﻿namespace AdminsPanel.ViewModels;

public class SpecialitiesLevelViewModel
{
    public string? Id { get; set; }
    public string? Name { get; set; }
    public string Description { get; set; }
    public string? SpecialityId  { get; set; }
}