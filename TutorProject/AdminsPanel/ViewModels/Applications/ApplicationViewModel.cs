﻿using AdminsPanel.ViewModels.Specializes;

namespace AdminsPanel.ViewModels.Applications;

public class ApplicationViewModel
{
    public string Email { get; set; }
    public IEnumerable<CertificateViewModel> Certificates { get; set; }
    public IEnumerable<ApplicationSpecialiseViewModel> ApplicationSpecialises { get; set; }
    public string Status { get; set; }
    public string? Description { get; set; }
}