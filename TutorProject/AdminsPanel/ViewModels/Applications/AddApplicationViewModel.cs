﻿namespace AdminsPanel.ViewModels.Applications;

public class AddApplicationViewModel
{
    public string Email { get; set; }
    public List<string> Checked { get; set; }
}