﻿namespace AdminsPanel.ViewModels.Applications;

public class ApplicationsViewModel
{
    public IEnumerable<ApplicationViewModel> Applications { get; set; }
}