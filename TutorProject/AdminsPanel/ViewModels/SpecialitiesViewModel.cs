﻿namespace AdminsPanel.ViewModels;

public class SpecialitiesViewModel
{ 
    public string Name { get; set; }
    public string Description { get; set; }
    public string SubjectName  { get; set; }
}