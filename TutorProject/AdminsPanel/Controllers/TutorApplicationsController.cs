﻿using AdminsPanel.Services.Application.Interfaces;
using AdminsPanel.ViewModels.Applications;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using StatusCodes = Responses.Enums.StatusCodes;

namespace AdminsPanel.Controllers;

public class TutorApplicationsController : Controller
{
    private readonly IApplicationService _applicationService;
    private readonly ITutorService _tutorService;

    public TutorApplicationsController(IApplicationService applicationService, ITutorService tutorService)
    {
        _applicationService = applicationService;
        _tutorService = tutorService;
    }

    [HttpPost("/TutorApplications/Create")]
    public async Task<ActionResult<DefaultResponseObject<StatusCodes>>> Create(TutorAddDto request)
    {
        var response = await _applicationService.Add(request);

        return Ok(response);
    }

    [HttpGet]
    public IActionResult GetAllApplications()
    {
        var applications = _applicationService.GetAllApplications();
        return View(applications);
    }

    [HttpGet]
    public async Task<IActionResult> ApplicationInformation(string email)
    {
        try
        {
            var application = await _applicationService.GetAllApplicationFromEmail(email);
        
            return View(application);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return View(null);
        }
    }

    [HttpPost("/TutorApplications/Add")]
    public async Task<IActionResult> Create(AddApplicationViewModel model)
    {
        var result = await _applicationService.CreateTutor(model);
        var response = await _tutorService.AddTutor(result);
        if (response.Value != StatusCodes.ServiceOk) return Ok(StatusCodes.ServiceIsUnavailable);
        
        var statusCode = await _applicationService.UpdateApplication(model);
        return Ok(statusCode);
    }
    
    [HttpPost("/TutorApplications/Refuse")]
    public async Task<IActionResult> RefuseTutor(RefuseTutorDto model)
    {
        var checkApplication = await _applicationService.CheckApplication(model);
        if (!checkApplication) return Ok(StatusCodes.UserExists);
        var response = await _tutorService.RefuseTutor(model);
        if (response.Value != StatusCodes.ServiceOk) return Ok(StatusCodes.ServiceIsUnavailable);
      
        var application = await _applicationService.RefuseTutor(model);
        return Ok(application);
    }
}