﻿using AdminsPanel.Services.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.ViewModels;

namespace AdminsPanel.Controllers;

public class SubjectsController : Controller
{
    private readonly ISubjectService _subjectService;

    public SubjectsController(ISubjectService subjectService)
    {
        _subjectService = subjectService;
    }

    [HttpGet("/Subjects/All")]
    public ActionResult<DefaultResponseObject<SubjectsDto>> GetAll() 
    {
        var subjects = _subjectService.GetAllSubjects();
        
        return Ok(new DefaultResponseObject<SubjectsDto> { Value = subjects });
    }
}