﻿using AdminsPanel.Entities;
using AdminsPanel.Enums;
using AdminsPanel.Helpers.Mappings;
using AdminsPanel.Repositories.PostgresRepository.Interfaces;
using AdminsPanel.Services.Application.Interfaces;
using AdminsPanel.ViewModels.Applications;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using StatusCodes = Responses.Enums.StatusCodes;

namespace AdminsPanel.Services.Application;

public class ApplicationService : IApplicationService
{
    private readonly IApplicationRepository _repository;
    private readonly IImageUploadService _imageUploadService;
    private readonly ISpecializeService _specializeService;
    public ApplicationService(IApplicationRepository repository, IImageUploadService imageUploadService, ISpecializeService specializeService)
    {
        _repository = repository;
        _imageUploadService = imageUploadService;
        _specializeService = specializeService;
    }

    public async Task<DefaultResponseObject<StatusCodes>> Add(TutorAddDto request)
    {
        if (request == null)
            return new DefaultResponseObject<StatusCodes>
            {
                Value = StatusCodes.UserNotFound
            };
        if (await _repository.AnyApplication(request.Email))
            return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.UserExists };
        
        await _imageUploadService.CreateAsync(request);
        
        var certificates = request.ImagesPath.MapImagesPath();
        if (certificates.Count == 0)
            return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceIsUnavailable };
        
        var applicationSpecialises = request.Selection.MapForm().ToList();
        if (applicationSpecialises.Count == 0)
            return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceIsUnavailable };
        
        TutorApplication tutorApplication = new TutorApplication
            { Certificates = certificates, Email = request.Email, 
                Status = ApplicationStatus.StatusNew,
                ApplicationSpecialises = applicationSpecialises,
                Description = request.Description
            };
        await _repository.CreateAsync(tutorApplication);
        await _repository.SaveAsync();

        return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceOk };
    }

    public ApplicationsViewModel GetAllApplications()
    {
        var applications = _repository.GetAllApplications();
        if (applications is null) return null;

        var applicationsList = applications.Map();
        if (applicationsList is null) return null;
        
        return new ApplicationsViewModel { Applications = applicationsList };
    }

    public async Task<ApplicationViewModel> GetAllApplicationFromEmail(string email)
    {
        var application = await _repository.GetApplicationStatusNewByEmailAsync(email);
        if (application.Certificates is null) return null;
        
        var applicationSpecialiseViewModels = application.ApplicationSpecialises.ApplicationSpecialisesMap();
        var certificateViewModels = application.Certificates.MapCertificates();
        string status = application.Status.MapStatus();

        var model = certificateViewModels.Map(applicationSpecialiseViewModels, status, application.Email,
            application.Description);
       
        return model;
    }

    public async Task<TutorCreateDto> CreateTutor(AddApplicationViewModel model)
    {
        try
        {
            if (model.Checked.Count == 0) return null;
            var application = await _repository.GetApplicationStatusNewByEmailAsync(model.Email);
        
            if (application is null) return null;
            
            var specializes = _specializeService.GetSpecialisesBySubjects(model.Checked);
            
            if (specializes is null) return null;
            return new TutorCreateDto { Email = model.Email, Specializes = specializes };
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return null;
        }
    }

    public async Task<StatusCodes> UpdateApplication(AddApplicationViewModel model)
    {
        if (model.Checked.Count == 0) return StatusCodes.ServiceIsUnavailable;
        var application = await _repository.GetApplicationStatusNewByEmailAsync(model.Email);
        application.Status = ApplicationStatus.StatusPassed;
        _repository.Update(application);
        await _repository.SaveAsync();
        return StatusCodes.ServiceOk;
    }

    public async Task<StatusCodes> RefuseTutor(RefuseTutorDto model)
    {
        var application = await _repository.GetApplicationStatusNewByEmailAsync(model.Email);
        if (application is null) return StatusCodes.UserNotFound;

        application.Status = ApplicationStatus.StatusNotPassed;
        _repository.Update(application);
        await _repository.SaveAsync();
        return StatusCodes.ServiceOk;
    }

    public Task<bool> CheckApplication(RefuseTutorDto model) => _repository.AnyApplication(model.Email);
}