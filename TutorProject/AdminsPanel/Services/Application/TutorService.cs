﻿using AdminsPanel.Services.Application.Interfaces;
using AdminsPanel.ViewModels.Applications;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Responses.Extensions;
using StatusCodes = Responses.Enums.StatusCodes;


namespace AdminsPanel.Services.Application;

public class TutorService : ITutorService
{
    private readonly HttpClient _httpClient;

    public TutorService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<DefaultResponseObject<StatusCodes>> AddTutor(TutorCreateDto model)
    {
        try
        {
            var response = await _httpClient.PostAndReturnResponseAsync<TutorCreateDto, StatusCodes>( model, "/Tutors/Create");
            return response;
        }
        catch (Exception e)
        {
            return new DefaultResponseObject<StatusCodes>{Value = StatusCodes.ServiceIsUnavailable};
        }
    }

    public async Task<DefaultResponseObject<StatusCodes>> RefuseTutor(RefuseTutorDto model)
    {
        try
        {
            var response = await _httpClient.PostAndReturnResponseAsync<RefuseTutorDto, StatusCodes>( model, "/Tutors/Refuse");
            return response;
        }
        catch (Exception e)
        {
            return new DefaultResponseObject<StatusCodes>{Value = StatusCodes.ServiceIsUnavailable};
        }
    }
}