﻿using AdminsPanel.Services.Application.Interfaces;
using Responses.DataTransferLib.TutorDtos;

namespace AdminsPanel.Services.Application.ImageServices
{
    public class ImageUploadService : IImageUploadService
    {
        private readonly UploadService _uploadService;
        private readonly IHostEnvironment _environment;
        private readonly List<string> _extensions;

        public ImageUploadService(UploadService uploadService, IHostEnvironment environment)
        {
            _uploadService = uploadService;
            _environment = environment;
            _extensions = new List<string> { ".jpg", ".png", ".pdf" };
        }
        
        public async Task CreateAsync(TutorAddDto entity)
        {
            string imagePath;
            entity.ImagesPath = new List<string>();
            if (entity.Files is null)
                return;
            
            foreach (var file in entity.Files)
            {
                if(Validate(file.FileName)) return;
                string dirPath = Path.Combine(_environment.ContentRootPath, $"wwwroot\\images\\certificates\\{entity.Email}\\{file.Name}");
                string guid = Guid.NewGuid().ToString();
                string fileName = $"{guid + file.FileName}";
                await _uploadService.UploadAsync(dirPath, fileName, file);
                imagePath = $"images\\certificates\\{entity.Email}\\files\\{fileName}";

                entity.ImagesPath.Add(imagePath);
            }
        }

        private bool Validate(string fileName)
        {
            fileName = fileName.Substring(fileName.Length - 4);
            return _extensions.All(extensions => extensions != fileName);
        }
    }
}