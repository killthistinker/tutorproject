﻿using Responses.ViewModels;

namespace AdminsPanel.Services.Application.Interfaces;

public interface ISubjectService
{
    public SubjectsDto GetAllSubjects();
}