﻿using AdminsPanel.ViewModels.Applications;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using StatusCodes = Responses.Enums.StatusCodes;
using TutorCreateDto = Responses.DataTransferLib.TutorDtos.TutorCreateDto;

namespace AdminsPanel.Services.Application.Interfaces;

public interface IApplicationService
{
    public Task<DefaultResponseObject<StatusCodes>> Add(TutorAddDto request);
    public ApplicationsViewModel GetAllApplications();
    public Task<ApplicationViewModel> GetAllApplicationFromEmail(string email);
    public Task<TutorCreateDto> CreateTutor(AddApplicationViewModel model);
    public Task<StatusCodes> UpdateApplication(AddApplicationViewModel model);
    public Task<StatusCodes> RefuseTutor(RefuseTutorDto model);
    public Task<bool> CheckApplication(RefuseTutorDto model);
}