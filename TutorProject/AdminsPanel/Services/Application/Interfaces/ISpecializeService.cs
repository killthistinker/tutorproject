﻿using AdminsPanel.ViewModels.Specializes;
using Responses.DataTransferLib.TutorDtos;

namespace AdminsPanel.Services.Application.Interfaces;

public interface ISpecializeService
{
    public GetAllSpecialisesViewModel  GetAllSpecialisesByValues(List<string> values);
    public IEnumerable<SpecialiseDto> GetSpecialisesBySubjects(List<string> subjects);
}