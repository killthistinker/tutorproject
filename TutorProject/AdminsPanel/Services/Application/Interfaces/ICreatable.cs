﻿namespace AdminsPanel.Services.Application.Interfaces
{
    public interface  ICreatable<in T> where T : class
    {
        public Task CreateAsync(T entity);
    }
}