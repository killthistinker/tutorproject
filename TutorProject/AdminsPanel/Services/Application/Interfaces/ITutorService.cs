﻿using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using StatusCodes = Responses.Enums.StatusCodes;

namespace AdminsPanel.Services.Application.Interfaces;

public interface ITutorService
{
    public Task<DefaultResponseObject<StatusCodes>> AddTutor(TutorCreateDto model);
    public Task<DefaultResponseObject<StatusCodes>> RefuseTutor(RefuseTutorDto model);
}