﻿using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;

namespace AdminsPanel.Services.Application.Interfaces
{
    public interface IImageUploadService : ICreatable<TutorAddDto>
    {
        
    }
}