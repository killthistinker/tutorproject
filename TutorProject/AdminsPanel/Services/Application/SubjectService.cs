﻿using AdminsPanel.Helpers.Mappings;
using AdminsPanel.Services.Application.Interfaces;
using Responses.ViewModels;
using TutorAdmin.Repositories;
using TutorAdmin.ViewModels;

namespace AdminsPanel.Services.Application;

public class SubjectService : ISubjectService
{
    private readonly IEntityRepository<SubjectsViewModel> _subjectRepository;

    public SubjectService(IEntityRepository<SubjectsViewModel> subjectRepository)
    {
        _subjectRepository = subjectRepository;
    }

    public SubjectsDto GetAllSubjects()
    {
        var subjects = _subjectRepository.GetEntitiesAsync();
        var subjectViewModel = subjects.Map();
        return subjectViewModel;
    }
}