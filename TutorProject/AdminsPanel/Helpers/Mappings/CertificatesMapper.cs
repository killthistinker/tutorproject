﻿using AdminsPanel.Entities;
using AdminsPanel.ViewModels.Applications;

namespace AdminsPanel.Helpers.Mappings;

public static class CertificatesMapper
{
    public static IEnumerable<CertificateViewModel> MapCertificates(this List<Certificate> certificates)
    {
        if (certificates.Count == 0) return new List<CertificateViewModel>();
        List<CertificateViewModel> certificateViewModels = new List<CertificateViewModel>();
        foreach (var certificate in certificates)
        {
            CertificateViewModel certificateViewModel = new CertificateViewModel { Path = certificate.Path };
            
            certificateViewModels.Add(certificateViewModel);
        }

        return certificateViewModels;
    }
}