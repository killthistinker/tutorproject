﻿using Responses.ViewModels;
using TutorAdmin.ViewModels;

namespace AdminsPanel.Helpers.Mappings;

public static class SubjectsViewModelHelper
{
    public static SubjectsDto Map(this IEnumerable<SubjectsViewModel> models)
    {
        List<string> subjectsViewModel = new List<string>();
        foreach (var model in models)
        {
            subjectsViewModel.Add(model.Name);
        }

        return new SubjectsDto { Subjects = subjectsViewModel };
    }
}