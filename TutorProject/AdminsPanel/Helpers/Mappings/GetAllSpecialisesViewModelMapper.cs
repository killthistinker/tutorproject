﻿using AdminsPanel.ViewModels;
using AdminsPanel.ViewModels.Specializes;

namespace AdminsPanel.Helpers.Mappings;

public static class GetAllSpecialisesViewModelMapper
{
    public static GetAllSpecialisesViewModel SpecialisesMapWithValues(this List<SpecialitiesViewModel>? models,
        List<string> values)
    {
        if (values.Count == 0) return null;
        List<SpecialitiesViewModel> modelsWithValues = new List<SpecialitiesViewModel>();
        foreach (var value in values)
        {
            if (models.Any(a => a.SubjectName == value))
            {
                var speciality = models.Where(a => a.SubjectName == value);
                modelsWithValues.AddRange(speciality);
            }
        }

        return new GetAllSpecialisesViewModel { Speicalities = modelsWithValues };
    }
}