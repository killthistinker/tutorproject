﻿using AdminsPanel.Entities;
using AdminsPanel.ViewModels.Applications;
using AdminsPanel.ViewModels.Specializes;

namespace AdminsPanel.Helpers.Mappings;

public static class ApplicationViewModelMapper
{
    public static List<ApplicationViewModel> Map(this IEnumerable<TutorApplication> applications)
    {
        List<ApplicationViewModel> applicationsList = new List<ApplicationViewModel>();
        foreach (var application in applications)
        {
            string status = application.Status.MapStatus();
            ApplicationViewModel applicationViewModel = new ApplicationViewModel
                { Email = application.Email, Status = status, Certificates = new List<CertificateViewModel>() };
            applicationsList.Add(applicationViewModel);
        }

        return applicationsList;
    }

    public static ApplicationViewModel Map(
        this IEnumerable<CertificateViewModel> certificateViewModels,
        IEnumerable<ApplicationSpecialiseViewModel> applicationSpecialiseViewModels,
        string status, string email, string description) =>
        new()
        {
            Certificates = certificateViewModels,
            ApplicationSpecialises = applicationSpecialiseViewModels,
            Description = description, Email = email,
            Status = status
        };

}