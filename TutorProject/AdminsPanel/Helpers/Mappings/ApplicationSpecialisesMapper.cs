﻿using AdminsPanel.Entities;
using AdminsPanel.ViewModels.Specializes;

namespace AdminsPanel.Helpers.Mappings;

public static class ApplicationSpecialisesMapper
{
    public static IEnumerable<ApplicationSpecialiseViewModel> ApplicationSpecialisesMap(this List<ApplicationSpecialise> applicationSpecialises)
    {
        List<ApplicationSpecialiseViewModel> applicationSpecialiseViewModels =
            new List<ApplicationSpecialiseViewModel>();
        foreach (var specialise in applicationSpecialises)
        {
            ApplicationSpecialiseViewModel applicationSpecialiseViewModel = new ApplicationSpecialiseViewModel
                { Specialise = specialise.Name };
            applicationSpecialiseViewModels.Add(applicationSpecialiseViewModel);
        }

        return applicationSpecialiseViewModels;
    }
}