﻿using AdminsPanel.Entities;

namespace AdminsPanel.Helpers.Mappings;

public static class ApplicationSpecialiseMapper
{
    public static IEnumerable<ApplicationSpecialise> MapForm(this List<string>? selections)
    {
        List<ApplicationSpecialise> applicationSpecialises = new List<ApplicationSpecialise>();
        if (selections is null) return new List<ApplicationSpecialise>();
        foreach (var selection in selections)
        {
            ApplicationSpecialise applicationSpecialise = new ApplicationSpecialise { Name = selection };
            applicationSpecialises.Add(applicationSpecialise);
        }

        return applicationSpecialises;
    }
}