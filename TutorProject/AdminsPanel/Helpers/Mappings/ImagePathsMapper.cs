﻿using AdminsPanel.Entities;

namespace AdminsPanel.Helpers.Mappings;

public static class ImagePathsMapper
{
    public static List<Certificate> MapImagesPath(this List<string> imagesPath)
    {
        if (imagesPath.Count == 0) return new List<Certificate>();
        
        List<Certificate> certificates = new List<Certificate>();
        foreach (var imagePath in imagesPath)
        {
            Certificate certificate = new Certificate { Path = imagePath };
            certificates.Add(certificate);
        }

        return certificates;
    }
}