﻿using AdminsPanel.Entities;
using Responses.Contracts.Repository;

namespace AdminsPanel.Repositories.PostgresRepository.Interfaces;

public interface IApplicationRepository : IRepository<TutorApplication>
{
    public IEnumerable<TutorApplication> GetAllApplications();
    public Task<TutorApplication> GetApplicationStatusNewByEmailAsync(string email);
    public Task<bool> AnyApplication(string email);
}