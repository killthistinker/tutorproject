﻿using AdminsPanel.Data.PostgresDb;
using AdminsPanel.Entities;
using AdminsPanel.Enums;
using AdminsPanel.Repositories.PostgresRepository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AdminsPanel.Repositories.PostgresRepository;

public class ApplicationRepository : IApplicationRepository
{
    private readonly TutorDbContext _context;

    public ApplicationRepository(TutorDbContext context)
    {
        _context = context;
    }

    public IEnumerable<TutorApplication>? GetAll() => _context.Applications;
    public async Task CreateAsync(TutorApplication item) => await _context.Applications.AddAsync(item);
    public void Create(TutorApplication item) => _context.Applications.Add(item);
    public void Update(TutorApplication item) => _context.Applications.Update(item);
    public void Remove(TutorApplication item) => _context.Applications.Remove(item);
    public void Save() => _context.SaveChanges();
    public async Task SaveAsync() => await _context.SaveChangesAsync();

    public IEnumerable<TutorApplication> GetAllApplications() =>
        _context.Applications.Where(a => a.Status == (int)ApplicationStatus.StatusNew);

    public async Task<TutorApplication> GetApplicationStatusNewByEmailAsync(string email) =>
        await _context.Applications.Include(a => a.Certificates).Include(a => a.ApplicationSpecialises).FirstOrDefaultAsync(a => a.Email == email && a.Status == ApplicationStatus.StatusNew);

    public async Task<bool> AnyApplication(string email) =>
        await _context.Applications.AnyAsync(s => s.Email == email && s.Status == ApplicationStatus.StatusNew);
}