using AdminsPanel.Data.MogoDb;
using AdminsPanel.Entities;
using AdminsPanel.Data.MongoDb;
using MongoDB.Driver;
using TutorAdmin.Repositories;
using TutorAdmin.ViewModels;

namespace AdminsPanel.Repositories.MongoRepository;

public class SubjectRepository : IEntityRepository<SubjectsViewModel>
{
    private readonly IEntityContext _context;

    public SubjectRepository(IEntityContext context)
    {
        _context = context;
    }
    public List<SubjectsViewModel> GetEntitiesAsync()
    {
        var subjects = _context.Subjects.Find(p => true).ToList();
        return subjects.Select(t => new SubjectsViewModel() { Id = t.Id, Description = t.Description, Name = t.Name }).ToList();
    }
    
    public List<SubjectsViewModel> GetEntitiesByName(string name)
    {
        var subjects = _context.Subjects.FindSync(c => c.Name.Equals(name)).ToList();
        List<SubjectsViewModel> models = subjects.Select(t => new SubjectsViewModel() { Id = t.Id, Description = t.Description, Name = t.Name }).ToList();
        return models;
    }
    
    public void CreateEntity(SubjectsViewModel entity)
    {
        Subject model = new Subject()
            {
                Name = entity.Name,
                Description = entity.Description
            };
        _context.Subjects.InsertOne(model);
    }

    public bool UpdateEntity(SubjectsViewModel entity)
    {
        // var updateResult = await _context.Subjects
        //     .ReplaceOneAsync(f => f.Id == entity.Id, entity);
        // return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        return false;
    }

    public bool DeleteEntity(string id)
    {
        // FilterDefinition<Subject> filter = Builders<Subject>.Filter.Eq(p => p.Id, id);
        // var deleteResult =  _context.Subjects.DeleteOneAsync(filter);
        // return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        return false;
    }
}