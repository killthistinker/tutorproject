using AdminsPanel.Data.MongoDb;
using AdminsPanel.Entities;
using AdminsPanel.ViewModels;
using MongoDB.Driver;
using TutorAdmin.Repositories;

namespace AdminsPanel.Repositories.MongoRepository;

public class SpecialityRepository : IEntityRepository<SpecialitiesViewModel>
{
    private readonly IEntityContext _context;

    public SpecialityRepository(IEntityContext context)
    {
        _context = context;
    }
    public  List<SpecialitiesViewModel> GetEntitiesAsync()
    {
        var specialities = _context.Specialities.Find(p => true).ToList();
        return specialities.Select(t => new SpecialitiesViewModel() { Description = t.Description, Name = t.Name, SubjectName = t.SubjectName}).ToList();
    }
    
    public List<SpecialitiesViewModel> GetEntitiesByName(string name)
    {
        var specialities = _context.Specialities.FindSync(c => c.Name.Equals(name)).ToList();
        List<SpecialitiesViewModel> models = specialities.Select(t => new SpecialitiesViewModel() {Description = t.Description, Name = t.Name, SubjectName = t.SubjectName}).ToList();
        return models;
    }

    public void CreateEntity(SpecialitiesViewModel entity)
    {
        if (entity.Name != null && entity.Description != null && entity.SubjectName != null)
        {
            Speciality model = new Speciality()
            {
                Name = entity.Name,
                Description = entity.Description,
                SubjectName = entity.SubjectName
            };
            _context.Specialities.InsertOne(model);
        }
    }

    public bool UpdateEntity(SpecialitiesViewModel entity)
    {
        // var updateResult =  _context.Specialities
        //     .ReplaceOneAsync(f => f.Id == entity.Id, entity);
        // return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        return false;
    }

    public bool DeleteEntity(string id)
    {
        // FilterDefinition<Speciality> filter = Builders<Speciality>.Filter.Eq(p => p.Id, id);
        // var deleteResult = await _context.Specialities.DeleteOneAsync(filter);
        // return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        return false;
    }
}