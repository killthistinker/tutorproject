﻿namespace TutorAdmin.Repositories;

public interface IEntityRepository <T>
{
    List<T> GetEntitiesAsync();
    List<T> GetEntitiesByName(string name);
    void CreateEntity(T entity);
    bool UpdateEntity(T entity);
    bool DeleteEntity(string id);
}