using AdminsPanel.Data;
using AdminsPanel.Data.PostgresDb;
using AdminsPanel.Repositories.MongoRepository;
using AdminsPanel.Repositories.PostgresRepository;
using AdminsPanel.Repositories.PostgresRepository.Interfaces;
using AdminsPanel.Services.Application;
using AdminsPanel.Services.Application.ImageServices;
using AdminsPanel.Services.Application.Interfaces;
using AdminsPanel.ViewModels;
using Microsoft.EntityFrameworkCore;
using AdminsPanel.Data.MongoDb;
using AdminsPanel.Validation;
using FluentValidation;
using TutorAdmin.Repositories;
using TutorAdmin.ViewModels;

var builder = WebApplication.CreateBuilder(args);


// Add services to the container.
builder.Services.AddControllersWithViews();
var services = builder.Services;
services.AddCors();
services.AddControllers();
services.AddDbContext<TutorDbContext>(opt =>
    opt.UseNpgsql(builder.Configuration.GetConnectionString("PostgresConnection")));
services.AddValidatorsFromAssemblyContaining(typeof(EntitiesVMValidator));
services.AddTransient<ISubjectService, SubjectService>();
services.AddTransient<ISpecializeService, SpecializeService>();
services.AddTransient<IImageUploadService, ImageUploadService>();
services.AddTransient<UploadService>();
services.AddScoped<IApplicationRepository, ApplicationRepository>();
services.AddTransient<IApplicationService, ApplicationService>();
services.AddTransient<IEntityContext, EntityContext>();
services.AddTransient<IEntityRepository<SpecialitiesViewModel>, SpecialityRepository>();
services.AddTransient<IEntityRepository<SubjectsViewModel>, SubjectRepository>();
services.AddTransient<ITutorService, TutorService>();
services.AddHttpClient<ITutorService, TutorService>(c => c.BaseAddress = new Uri(builder.Configuration["ApiSettings:GatewayUrl"]));
services.AddEndpointsApiExplorer();
services.AddSwaggerGen(s =>
{
    s.EnableAnnotations();
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();
app.UseCors(builder => builder.AllowAnyOrigin());
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Entities}/{action=GetEntity}/");

app.Run();