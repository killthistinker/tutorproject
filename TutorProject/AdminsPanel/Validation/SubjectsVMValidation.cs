﻿using FluentValidation;
using TutorAdmin.ViewModels;

namespace AdminsPanel.Validation;

public class SubjectsVMValidation : AbstractValidator<SubjectsViewModel>
{
    public SubjectsVMValidation()
    {
        RuleFor(p => p.Name).NotEmpty().MinimumLength(2).MaximumLength(20).WithMessage("От 2 до 20 символов");
        RuleFor(p => p.Description).NotEmpty().MinimumLength(10).MaximumLength(150);
    }
}