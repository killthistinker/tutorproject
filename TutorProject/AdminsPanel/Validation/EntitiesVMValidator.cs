﻿using FluentValidation;
using TutorAdmin.ViewModels;

namespace AdminsPanel.Validation;

public class EntitiesVMValidator : AbstractValidator<EntitiesViewModel>
{
    public EntitiesVMValidator()
    {
        RuleForEach(p => p.Subjects).SetValidator(new SubjectsVMValidation());
        RuleForEach(p => p.Specialities).SetValidator(new SpecialitiesVMValidator());
    }
}