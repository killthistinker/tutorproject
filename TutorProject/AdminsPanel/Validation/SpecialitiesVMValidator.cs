﻿using AdminsPanel.ViewModels;
using FluentValidation;

namespace AdminsPanel.Validation;

public class SpecialitiesVMValidator : AbstractValidator<SpecialitiesViewModel>
{
    public SpecialitiesVMValidator()
    {
        RuleFor(p => p.Name).MaximumLength(15).MinimumLength(2).WithMessage("От 2 до 15 символов");
        RuleFor(p => p.Description).MaximumLength(15).MinimumLength(2);
    }
}