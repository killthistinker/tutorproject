﻿namespace AdminsPanel.Entities;

public class ApplicationSpecialise
{
    public long Id { get; set; }
    public string Name { get; set; }
}