﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AdminsPanel.Entities;

public class Speciality
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string SubjectName { get; set; }
}