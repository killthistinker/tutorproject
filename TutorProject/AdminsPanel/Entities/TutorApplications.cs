﻿using AdminsPanel.Enums;

namespace AdminsPanel.Entities;

public class TutorApplication
{
    public long Id { get; set; }
    public string Email { get; set; }
    public string? Description { get; set; }
    public List<ApplicationSpecialise> ApplicationSpecialises { get; set; }
    public List<Certificate> Certificates { get; set; }
    public ApplicationStatus Status { get; set; }

    public TutorApplication()
    {
        Certificates = new List<Certificate>();
        ApplicationSpecialises = new List<ApplicationSpecialise>();
    }
}