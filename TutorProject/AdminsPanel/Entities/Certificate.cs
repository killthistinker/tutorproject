﻿namespace AdminsPanel.Entities;

public class Certificate
{
    public long Id { get; set; }
    public string Path { get; set; }
}