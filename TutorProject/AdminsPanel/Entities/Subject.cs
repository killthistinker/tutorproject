﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AdminsPanel.Entities;

public class Subject
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Name { get; set; }
    public string? Description { get; set; }
}