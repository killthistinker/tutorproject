﻿// <auto-generated />
using System;
using AdminsPanel.Data.PostgresDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace AdminsPanel.Migrations
{
    [DbContext(typeof(TutorDbContext))]
    [Migration("20221120164959_ApplicationSpecialises")]
    partial class ApplicationSpecialises
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.11")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("AdminsPanel.Entities.ApplicationSpecialise", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<long?>("TutorApplicationId")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("TutorApplicationId");

                    b.ToTable("ApplicationSpecialise");
                });

            modelBuilder.Entity("AdminsPanel.Entities.Certificate", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<string>("Path")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<long?>("TutorApplicationId")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("TutorApplicationId");

                    b.ToTable("Certificate");
                });

            modelBuilder.Entity("AdminsPanel.Entities.TutorApplication", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("Status")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("Applications");
                });

            modelBuilder.Entity("AdminsPanel.Entities.ApplicationSpecialise", b =>
                {
                    b.HasOne("AdminsPanel.Entities.TutorApplication", null)
                        .WithMany("ApplicationSpecialises")
                        .HasForeignKey("TutorApplicationId");
                });

            modelBuilder.Entity("AdminsPanel.Entities.Certificate", b =>
                {
                    b.HasOne("AdminsPanel.Entities.TutorApplication", null)
                        .WithMany("Certificates")
                        .HasForeignKey("TutorApplicationId");
                });

            modelBuilder.Entity("AdminsPanel.Entities.TutorApplication", b =>
                {
                    b.Navigation("ApplicationSpecialises");

                    b.Navigation("Certificates");
                });
#pragma warning restore 612, 618
        }
    }
}
