﻿using AutoMapper;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Responses.Enums;
using Tutor.Application.Services.Interfaces;
using Tutor.Domain.Entities;
using Tutor.Domain.Models.Dto;
using Tutor.Infrastructure.Contracts.Repository.Interfaces;

namespace Tutor.Application.Services;

public class TutorService : ITutorService
{
    private readonly ITutorRepository _repository;
    private readonly IMapper _mapper;
    private readonly IEmailSendService _emailSendService;

    public TutorService(ITutorRepository repository, IMapper mapper, IEmailSendService emailSendService)
    {
        _repository = repository;
        _mapper = mapper;
        _emailSendService = emailSendService;
    }
    
    public DefaultResponseObject<IEnumerable<TutorDto>> GetAll()
    {
        var tutors = _repository.GetAll();
        if (tutors is null)
            return new DefaultResponseObject<IEnumerable<TutorDto>>
            {
                Value = null,
                ErrorModel = new ErrorModel { Message = "Список учителей пуст", StatusCode = StatusCodes.UserNotFound }
            };

        var objects = _mapper.Map<IEnumerable<Domain.Entities.Tutor>, IEnumerable<TutorDto>>(tutors);

        return new DefaultResponseObject<IEnumerable<TutorDto>>
        {
            Value = objects
        };
    }

    public async Task<DefaultResponseObject<StatusCodes>> CreateTutor(TutorCreateDto model)
    {
        List<Specialise> specialises = new List<Specialise>();
        foreach (var modelSpecialize in model.Specializes)
        {
            Specialise specialise = new Specialise
                { Name = modelSpecialize.Name, SubjectName = modelSpecialize.SubjectName };
            specialises.Add(specialise);
        }

        Domain.Entities.Tutor tutor = new Domain.Entities.Tutor { Email = model.Email, Specialises = specialises };
        await _repository.CreateAsync(tutor);
        await _repository.SaveAsync();
        await _emailSendService.SendEmailTutorNotification(model);
        return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceOk };
    }

    public async Task<DefaultResponseObject<StatusCodes>> ResponseTutor(RefuseTutorDto model)
    {
        await _emailSendService.SendRefuseEmail(model);
        return new DefaultResponseObject<StatusCodes>{ Value = StatusCodes.ServiceOk };
    }
}