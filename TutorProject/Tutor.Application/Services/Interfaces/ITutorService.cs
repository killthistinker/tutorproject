﻿using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Responses.Enums;
using Tutor.Domain.Models.Dto;

namespace Tutor.Application.Services.Interfaces;

public interface ITutorService
{
    public DefaultResponseObject<IEnumerable<TutorDto>> GetAll();
    public Task<DefaultResponseObject<StatusCodes>> CreateTutor(TutorCreateDto model);
    public Task<DefaultResponseObject<StatusCodes>> ResponseTutor(RefuseTutorDto model);
}