﻿using System.Text;
using MailKit.Net.Smtp;
using MimeKit;
using Responses.DataTransferLib.TutorDtos;
using Tutor.Application.Services.Interfaces;

namespace Tutor.Application.Services
{
    public class EmailSendService : IEmailSendService
    {
        private readonly string _emailSubject;
        private readonly string _tutorConfirmMessage;
        private readonly string _tutorRefuseMessage;
        private readonly StringBuilder _message;

        public EmailSendService()
        {
            _tutorRefuseMessage = "Отказ на репетиторство";
            _tutorConfirmMessage = "Поздравляем! Вы стали репетитором Tutor!";
            _emailSubject = "Ответ на заявку репетиторства";
            _message = new StringBuilder();
        }
        
        public async Task SendEmailTutorNotification(TutorCreateDto model)
        {
            var emailMessage = new MimeMessage();
            
            _message.Append($"<h1>{_tutorConfirmMessage}</h1>");
            _message.Append($"<h2>Ваши специальности: </h2>");
            foreach (var specialise in model.Specializes)
            {
                _message.Append($"<p>Предмет:{specialise.SubjectName}.Специальность:{specialise.Name}</p>");
            }
            
            emailMessage.From.Add(new MailboxAddress("Администрация сайта Tutor", "killthistinker@mail.ru"));
            emailMessage.To.Add(new MailboxAddress("", model.Email));
            emailMessage.Subject = _emailSubject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = _message.ToString(),
            };

            await SendEmailAsync(emailMessage);
        }

        public async Task SendEmailAsync(MimeMessage emailMessage)
        {
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.mail.ru", 25, false);
                await client.AuthenticateAsync("killthistinker@mail.ru", "wDZeMYAr04cDPfrEfaP2");
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }

        public async Task SendRefuseEmail(RefuseTutorDto model)
        {
            var emailMessage = new MimeMessage();
            
            _message.Append($"<h1>{_tutorRefuseMessage}</h1>");
            _message.Append($"<h2>Причина отказа: {model.Message} </h2>");
           
            
            emailMessage.From.Add(new MailboxAddress("Администрация сайта Tutor", "killthistinker@mail.ru"));
            emailMessage.To.Add(new MailboxAddress("", model.Email));
            emailMessage.Subject = _emailSubject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = _message.ToString(),
            };
            await SendEmailAsync(emailMessage);
        }
    }
}