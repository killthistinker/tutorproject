﻿using AutoMapper;
using Tutor.Domain.Models.Dto;

namespace Tutor.Application.Mappings;

public class TutorMapper : Profile
{
    public TutorMapper()
    {
        CreateMap<IEnumerable<TutorDto>, IEnumerable<Domain.Entities.Tutor>>();
    }
}