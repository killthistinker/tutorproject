﻿using Microsoft.Extensions.DependencyInjection;
using Tutor.Application.Mappings;
using Tutor.Application.Services;
using Tutor.Application.Services.Interfaces;

namespace Tutor.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<ITutorService, TutorService>();
        serviceCollection.AddTransient<IEmailSendService, EmailSendService>();
        serviceCollection.AddAutoMapper(typeof(TutorMapper));
        return serviceCollection;
    }
}