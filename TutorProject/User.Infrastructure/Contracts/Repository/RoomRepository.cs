﻿using Microsoft.EntityFrameworkCore;
using User.Domain.Entities;
using User.Infrastructure.Contracts.Repository.Interfaces;
using User.Infrastructure.Data;

namespace User.Infrastructure.Contracts.Repository;

public class RoomRepository : IRoomRepository
{
    private readonly TutorDbContext _context;

    public RoomRepository(TutorDbContext context)
    {
        _context = context;
    }

    public IEnumerable<Room>? GetAll() => _context.Rooms.Include(c => c.Creator);
    public async Task CreateAsync(Room item) => await _context.Rooms.AddAsync(item);
    public void Create(Room item) => _context.Rooms.Add(item);
    public void Update(Room item) => _context.Rooms.Update(item);
    public void Remove(Room item) => _context.Rooms.Remove(item);
    public void Save() => _context.SaveChanges();
    public async Task SaveAsync() => await _context.SaveChangesAsync();
}