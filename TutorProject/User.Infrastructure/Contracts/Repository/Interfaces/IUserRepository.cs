﻿using Responses.Contracts.Repository;

namespace User.Infrastructure.Contracts.Repository.Interfaces;

public interface IUserRepository : IRepository<Domain.Entities.User>
{
    public Task<Domain.Entities.User?>? GetFirstOrDefaultByIdAsync(long id);
    public Task<Domain.Entities.User?>? GetFirstOrDefaultByEmailAsync(string email);
}