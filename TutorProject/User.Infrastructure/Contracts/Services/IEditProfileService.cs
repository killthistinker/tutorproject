﻿using Ardalis.Result;
using Responses.AccountModels;
using Responses.DataTransferLib;

namespace User.Infrastructure.Contracts.Services
{
    public interface IEditProfileService
    {
        Task<Result<DefaultResponseObject<DefaultResponse>>> EditProfileAsync(EditUserDto userModel);
        Task<Result<DefaultResponseObject<DefaultResponse>>> EditPasswordAsync(string email, string password, string confirmPassword);
    }
}
