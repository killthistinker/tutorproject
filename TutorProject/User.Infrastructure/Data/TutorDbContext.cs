﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using User.Domain.Entities;

namespace User.Infrastructure.Data;

public class TutorDbContext : IdentityDbContext<Domain.Entities.User, Role, long>
{
    public DbSet<Domain.Entities.User>? Users { get; set; }
    public DbSet<Room>? Rooms { get; set; }

    public TutorDbContext(DbContextOptions<TutorDbContext> options) : base(options)
    {
        
    }
}