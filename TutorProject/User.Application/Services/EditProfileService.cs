﻿using Ardalis.Result;
using Ardalis.Result.FluentValidation;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Responses.AccountModels;
using Responses.DataTransferLib;
using User.Infrastructure.Contracts.Services;

namespace User.Application.Services
{
    public class EditProfileService : IEditProfileService
    {
        private readonly UserManager<User.Domain.Entities.User> _userManager;
        private readonly IMapper _mapper;
        private readonly IValidator<EditUserDto> _editUserValidator;

        public EditProfileService(IValidator<EditUserDto> editUserValidator, UserManager<User.Domain.Entities.User> userManager, IMapper mapper)
        {
            _editUserValidator = editUserValidator;
            _userManager = userManager;
            _mapper = mapper;
        }

        public Task<Result<DefaultResponseObject<DefaultResponse>>> EditPasswordAsync(string email, string password, string confirmPassword)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<DefaultResponseObject<DefaultResponse>>> EditProfileAsync(EditUserDto userModel)
        {
            var validationResult = await _editUserValidator.ValidateAsync(userModel);
            if (!validationResult.IsValid) return Result<DefaultResponseObject<DefaultResponse>>.Invalid(validationResult.AsErrors());

            var test = await _userManager.FindByEmailAsync(userModel.Email);

            if (userModel.Password != "" & userModel.PasswordConfirm != "")
            {
            }
            if (userModel.Surname != null)
            {
                test.LastName = userModel.Surname;
            }
            if (userModel.Name != null)
            {
                test.FirstName = userModel.Name;
            }
            if (userModel.Phone != null)
            {
                test.PhoneNumber = userModel.Phone;
            }       

            var result = await _userManager.UpdateAsync(test);
           
            if (!result.Succeeded) return new DefaultResponseObject<DefaultResponse> { Value = new DefaultResponse { IsResult = false } };

            return new DefaultResponseObject<DefaultResponse> { Value = new DefaultResponse { IsResult = true } };
        }
    }
}
