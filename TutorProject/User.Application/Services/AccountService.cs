using Ardalis.Result;
using Ardalis.Result.FluentValidation;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Responses.AccountModels;
using Responses.AccountModels.Responses;
using Responses.DataTransferLib;
using User.Infrastructure.Contracts.Services;

namespace User.Application.Services;

public class AccountService : IAccountService
{
    private readonly UserManager<Domain.Entities.User> _userManager;
    private readonly IValidator<UserRegisterDto> _registerValidator;
    private readonly IMapper _mapper;
    private readonly ILogger<AccountService> _logger;

    public AccountService(UserManager<Domain.Entities.User> userManager, IMapper mapper, IValidator<UserRegisterDto> registerValidator, ILogger<AccountService> logger)
    {
        _userManager = userManager;
        _mapper = mapper;
        _registerValidator = registerValidator;
        _logger = logger;
    }
    
    public async Task<Result<DefaultResponseObject<AuthenticateResponse>>> Authenticate(UserAuthDto model)
    {
        var user = await _userManager.FindByEmailAsync(model.Email);
        if (user is null) return new DefaultResponseObject<AuthenticateResponse>{ErrorModel = {Message = "Неверное имя пользователя или пароль"}};

        return new DefaultResponseObject<AuthenticateResponse>
        {
            Value = new AuthenticateResponse { Email = user.Email },
            IsSuccess = true
        };
    }
    
    public async Task<Result<DefaultResponseObject<DefaultResponse>>> Register(UserRegisterDto userModel)
    {
        var validationResult = await _registerValidator.ValidateAsync(userModel);
        if (!validationResult.IsValid)
            return Result<DefaultResponseObject<DefaultResponse>>.Invalid(validationResult.AsErrors());

        Domain.Entities.User user = _mapper.Map<Domain.Entities.User>(userModel);
        var result = await _userManager.CreateAsync(user, userModel.Password);
        if (!result.Succeeded)
            return new DefaultResponseObject<DefaultResponse> { Value = new DefaultResponse { IsResult = false } };
        
        return new DefaultResponseObject<DefaultResponse> { Value = new DefaultResponse { IsResult = true } };
    }
}