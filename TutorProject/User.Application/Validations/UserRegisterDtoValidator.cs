﻿using FluentValidation;
using Responses.AccountModels;
using User.Infrastructure.Contracts.Repository.Interfaces;

namespace User.Application.Validations;

public class UserRegisterDtoValidator : AbstractValidator<UserRegisterDto>
{
    private readonly IUserRepository _repository;
    public UserRegisterDtoValidator(IUserRepository repository)
    {
        _repository = repository;
        RuleFor(p => p.Email)
            .MustAsync(IsUniqueEmail).WithMessage("Email занят")
            .EmailAddress().WithMessage("Неверный формат ввода");
        RuleFor(p => p.Password)
            .MinimumLength(5).WithMessage("Неверный формат пароля").NotNull()
            .WithMessage("Поле не может быть пустым");
        RuleFor(p => p.Phone)
            .MinimumLength(11).WithMessage("Минимальная длинна 11 символов")
            .NotNull().WithMessage("Поле не может быть пустым")
            .MaximumLength(18).WithMessage("Максимальная длинна 18 символов");
        RuleFor(p => p.DateBirth)
            .NotNull().WithMessage("Поле не может быть пустым");
        RuleFor(p => p.FirstName)
            .NotNull().WithMessage("Поле не может быть пустым")
            .MaximumLength(40).WithMessage("Имя не должно быть длинной больше 40 символов");
        RuleFor(p => p.LastName)
            .NotNull().WithMessage("Поле не может быть пустым")
            .MaximumLength(40).WithMessage("Имя не должно быть длинной больше 40 символов");;
        RuleFor(p => p.PasswordConfirm).Matches(p => p.Password).WithMessage("Пароли не совпадают");
    }

    private async Task<bool> IsUniqueEmail(string email, CancellationToken arg2)
    {
        return await _repository.GetFirstOrDefaultByEmailAsync(email) is null;
    }
}