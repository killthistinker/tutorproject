﻿using FluentValidation;
using Responses.AccountModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User.Infrastructure.Contracts.Repository.Interfaces;

namespace User.Application.Validations
{
    public class EditPasswordDtoValidator : AbstractValidator<EditUserDto>
    {
        private readonly IUserRepository _repository;

        public EditPasswordDtoValidator(IUserRepository repository)
        {
            _repository = repository;
            RuleFor(p => p.Phone)
                .MinimumLength(11).WithMessage("Минимальная длинна 11 символов")
                .NotNull().WithMessage("Поле не может быть пустым")
                .MaximumLength(18).WithMessage("Максимальная длинна 18 символов");
            RuleFor(p => p.Name)
                .NotNull().WithMessage("Поле не может быть пустым")
                .MaximumLength(40).WithMessage("Имя не должно быть длинной больше 40 символов");
            RuleFor(p => p.Surname)
                .NotNull().WithMessage("Поле не может быть пустым")
                .MaximumLength(40).WithMessage("Имя не должно быть длинной больше 40 символов");
        }
    }
}
