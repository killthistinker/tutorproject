using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Payment.Domain;
using Payment.Domain.Entities;
using Payment.Infrastructure;
using Payment.Interfaces;
using Responses.AccountModels;
using Responses.DataTransferLib;
using Responses.Payments;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Payment.API.Endpoints.Paybox;


[ApiController]
[Route("[controller]")]
public class GetPagePayboxEndpoint : EndpointBaseSync.WithRequest<PaymentDTO>.WithActionResult<DefaultResponseObject<PayResponse>>
{
    private readonly IPayboxIntegration _paybox;
    private readonly IPaymentRepository _repository;
    public GetPagePayboxEndpoint(IPayboxIntegration paybox, IPaymentRepository repository)
    {
        _paybox = paybox;
        _repository = repository;
    }
    [HttpPost("/Payment/GetPage")]

    public override ActionResult<DefaultResponseObject<PayResponse>> Handle(PaymentDTO model)
    {
        var data = new TransactionLog()
        {
            CreateDate = DateTime.Now,
            LessonDate = model.LessonDate,
            Amount = model.Amount,
            LessonName = model.LessonName,
            Status = "partial",
            Email = model.Email,
            PayboxId = 0,
            CheckCount = 0

        };
        
        _repository.CreateLog(data);
        _repository.Save();
        model.TransactionId = data.Id.ToString();
        var result  =_paybox.GetUrlPaymentAsync(model);
        if (result.Result.IsSuccess)
        {
            data.PayboxId = result.Result.Value.PaymentId;
            data.Status = "pending";
        }
        _repository.UpdateLog(data);
        _repository.Save();
        return Ok(new DefaultResponseObject<PayResponse>()
            {IsSuccess = result.Result.IsSuccess,
                Value = new PayResponse()
                {Response = result.Result.Value.RedirectUrl ?? result.Result.Value.ErrorDescription}});
    }
}