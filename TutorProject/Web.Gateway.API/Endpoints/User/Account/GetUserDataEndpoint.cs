﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using StatusCodes = Responses.Enums.StatusCodes;

namespace WebGatewayAPI.Endpoints.User.Account;

[ApiController]
[Route("[controller]")]
public class GetUserDataEndpoint : EndpointBaseSync.WithoutRequest.WithActionResult
{

    [HttpGet("/Account/GetUserData")]
    public override ActionResult Handle()
    {
        try
        {
            string value = HttpContext.Session.GetString("user") ?? throw new InvalidOperationException();
            return Ok(value);
        }
        catch (Exception)
        {
            return Ok(new ErrorModel{Message = "User is not Authorized", StatusCode = StatusCodes.UserIsNotAuthorized});
        }
    }
}