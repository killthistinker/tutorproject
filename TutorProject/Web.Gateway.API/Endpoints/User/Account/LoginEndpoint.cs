﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.AccountModels;
using Responses.AccountModels.Responses;
using Responses.DataTransferLib;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace WebGatewayAPI.Endpoints.User.Account;

[ApiController]
[Route("[controller]")]
public class LoginEndpoint : EndpointBaseAsync.WithRequest<UserAuthDto>.WithActionResult<DefaultResponseObject<AuthenticateResponse>>
{
  
    private readonly IUserService _userService;
    private readonly ILogger<LoginEndpoint> _logger;

    public LoginEndpoint(IUserService userService, ILogger<LoginEndpoint> logger)
    {
        _userService = userService;
        _logger = logger;
    }

    [HttpPost("/Account/Login")]
    public override async Task<ActionResult<DefaultResponseObject<AuthenticateResponse>>> HandleAsync(UserAuthDto request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var result = await _userService.LoginAsync(request, cancellationToken);
            if (result.IsSuccess) HttpContext.Session.SetString("user", request.Email);
            return Ok(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Произошла ошибка");
            return Ok(StatusCodes.ServiceIsUnavailable);
        }
    }
}