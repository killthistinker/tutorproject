﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.AccountModels;
using Responses.DataTransferLib;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace WebGatewayAPI.Endpoints.User.Account;

[ApiController]
[Route("[controller]")]
public class RegisterEndpoint : EndpointBaseAsync.WithRequest<UserRegisterDto>.WithActionResult<DefaultResponseObject<DefaultResponse>>
{
    private readonly IUserService _userService;
    private readonly ILogger<RegisterEndpoint> _logger;

    public RegisterEndpoint(IUserService userService, ILogger<RegisterEndpoint> logger)
    {
        _userService = userService;
        _logger = logger;
    }
    [HttpPost("/Account/Register")]
    public override async Task<ActionResult<DefaultResponseObject<DefaultResponse>>> HandleAsync(UserRegisterDto request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var result = await _userService.RegisterAsync(request, cancellationToken);
        
            return Ok(result);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Произошла ошибка");
            return Ok(StatusCodes.ServiceIsUnavailable);
        }
    }
}