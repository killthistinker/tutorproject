﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;

namespace WebGatewayAPI.Endpoints.User.Account;

public class LogOutEndpoint : EndpointBaseSync.WithoutRequest.WithActionResult
{
    [HttpPost("/Account/LogOut")]
    public override ActionResult Handle()
    {
        HttpContext.Session.Remove("user");
        return Ok();
    }
}