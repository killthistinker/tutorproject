﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.Tutor;

public class RefuseTutorEndpoint : EndpointBaseAsync.WithRequest<RefuseTutorDto>.WithActionResult<DefaultResponseObject<StatusCodes>>
{
    private readonly ITutorService _service;
    private readonly ILogger<RefuseTutorEndpoint> _logger;

    public RefuseTutorEndpoint(ITutorService service, ILogger<RefuseTutorEndpoint> logger)
    {
        _service = service;
        _logger = logger;
    }

    [HttpPost("/Tutors/Refuse")]
    public override async Task<ActionResult<DefaultResponseObject<StatusCodes>>> HandleAsync(RefuseTutorDto request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var response = await _service.RefuseTutor(request);
            return Ok(response);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath");
            return Ok(new DefaultResponseObject<StatusCodes>{Value = StatusCodes.ServiceIsUnavailable});
        }
    }
}