﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.Room;

public class GetAllRoomEndpoint : EndpointBaseAsync.WithoutRequest.WithActionResult<DefaultResponseObject<IEnumerable<RoomDto>>>
{
    private readonly IRoomService _roomService;
    private readonly ILogger<GetAllRoomEndpoint> _logger;

    public GetAllRoomEndpoint(IRoomService roomService, ILogger<GetAllRoomEndpoint> logger)
    {
        _roomService = roomService;
        _logger = logger;
    }

    [HttpGet("/Room/All")]
    public override async Task<ActionResult<DefaultResponseObject<IEnumerable<RoomDto>>>> HandleAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var email = HttpContext.Session.GetString("user");
            if (email is null) return 
                Ok(new DefaultResponseObject<IEnumerable<RoomDto>>
                {
                    ErrorModel = new ErrorModel{Message = "Пользователь не найден",StatusCode = StatusCodes.UserIsNotAuthorized},
                });

            var response = await _roomService.GetAll();

            return Ok(response);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath");
            return Ok(new DefaultResponseObject<IEnumerable<RoomDto>>
            {
                ErrorModel = new ErrorModel
                    { Message = "Сервис временно недоступен", StatusCode = StatusCodes.ServiceIsUnavailable },
            });
        }
    }
}