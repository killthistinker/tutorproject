﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.Room;

public class AddRoomEndpoint : EndpointBaseAsync.WithRequest<RoomModelRequest>.WithActionResult<DefaultResponseObject<StatusCodes>>
{
    private readonly IRoomService _roomService;
    private readonly ILogger<AddRoomEndpoint> _logger;

    public AddRoomEndpoint(IRoomService roomService, ILogger<AddRoomEndpoint> logger)
    {
        _roomService = roomService;
        _logger = logger;
    }

    [HttpPost("/Room/Add")]
    public override async Task<ActionResult<DefaultResponseObject<StatusCodes>>> HandleAsync(RoomModelRequest request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var email = HttpContext.Session.GetString("user");
            if (email is null) return Ok(StatusCodes.UserNotFound);

            var response = await _roomService.Add(request, email);
            return Ok(response);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath.");
            return Ok(StatusCodes.ServiceIsUnavailable);
        }
    }
}