﻿using System.Text.Json;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.AccountModels;
using Responses.DataTransferLib;
using Responses.DataTransferLib.ScheduleDTO;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.API.Endpoints.Schedule;

public class AddTutorPeriodEndPoint : EndpointBaseAsync.WithRequest<JsonDocument>.WithActionResult<DefaultResponseObject<DefaultResponse>>
{
    private readonly IScheduleService _scheduleService;

    public AddTutorPeriodEndPoint(IScheduleService scheduleService)
    {
        _scheduleService = scheduleService;
    }
    
    [HttpPost("AddAvailablePeriod")]
    
    public override async Task<ActionResult<DefaultResponseObject<DefaultResponse>>> HandleAsync([FromBody]JsonDocument? request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            if (request != null)
            {
                var data = System.Text.Json.JsonSerializer.Deserialize<SelectedHoursDto>(request);
                var response = await _scheduleService.AddAvailablePeriod(data);
                return Ok(response);
            }

            return Ok(StatusCode(404));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}