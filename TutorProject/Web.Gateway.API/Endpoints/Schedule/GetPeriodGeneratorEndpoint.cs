﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.DataTransferLib.ScheduleDTO;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.Schedule;

public class GetPeriodGenerator : EndpointBaseAsync.WithoutRequest.WithActionResult<DefaultResponseObject<EmptyPeriodDto>>
{
    private readonly IScheduleService _scheduleService;
    // private readonly ILogger<GetAllRoomEndpoint> _logger;

    public GetPeriodGenerator(IScheduleService scheduleService)
    {
        _scheduleService = scheduleService;
    }
    
    [HttpGet("GetEmptyPeriod")]
    public override async Task<ActionResult<DefaultResponseObject<EmptyPeriodDto>>> HandleAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            // var email = HttpContext.Session.GetString("user");
            // if (email is null) return 
            //     Ok(new DefaultResponseObject<IEnumerable<EmptyPeriodDto>>
            //     {
            //         ErrorModel = new ErrorModel{Message = "Пользователь не найден",StatusCode = StatusCodes.UserIsNotAuthorized},
            //     });

            var response = await _scheduleService.GetGetEmptyPeriod();

            return Ok(response);
        }
        catch (Exception e)
        {
            return Ok(new DefaultResponseObject<IEnumerable<EmptyPeriodDto>>
            {
                ErrorModel = new ErrorModel
                    { Message = "Сервис временно недоступен", StatusCode = StatusCodes.ServiceIsUnavailable },
            });
        }
    }
}