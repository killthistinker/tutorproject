﻿using Web.Gateway.Infrastructure.Mappings;
using Web.Gateway.Infrastructure.Services;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.API;

public static class DependencyInjection
{
    public static IServiceCollection AddHttpClients(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddHttpClient<IUserService, UserService>(c => c.BaseAddress = new Uri(configuration["ApiSettings:IdentityUrl"]));
        services.AddHttpClient<IRoomService, RoomService>(c => c.BaseAddress = new Uri(configuration["ApiSettings:IdentityUrl"]));
        services.AddHttpClient<ITutorService, TutorService>(c => c.BaseAddress = new Uri(configuration["ApiSettings:TutorUrl"]));
        services.AddHttpClient<IScheduleService, ScheduleService>(c => c.BaseAddress = new Uri(configuration["ApiSettings:ScheduleUrl"]));
        services.AddHttpClient<IPaymentService, PaymentService>(c => c.BaseAddress = new Uri(configuration["ApiSettings:PaymentUrl"]));
        services.AddHttpClient<ITutorApplicationService, TutorApplicationService>(c =>
            c.BaseAddress = new Uri(configuration["ApiSettings:AdminPanelUrl"]));
        services.AddHttpClient<ISubjectService, SubjectService>(c =>
            c.BaseAddress = new Uri(configuration["ApiSettings:AdminPanelUrl"]));
        services.AddAutoMapper(typeof(RoomRequestMapper));
        return services;
    }
}