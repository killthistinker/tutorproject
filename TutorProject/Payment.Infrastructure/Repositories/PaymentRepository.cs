using Payment.Domain.Entities;
using Payment.Infrastructure.Data;
using Payment.Interfaces;

namespace Payment.Infrastructure;

public class PaymentRepository : IPaymentRepository
{
    private readonly TutorDbContext _db;

    public PaymentRepository(TutorDbContext db)
    {
        _db = db;
    }

    public void CreateLog(TransactionLog log)
    {
        _db.Logs.Add(log);
    }

    public void UpdateLog(TransactionLog log)
    {
        _db.Logs.Update(log);
    }

    public List<TransactionLog> GetAll()
    {
       return _db.Logs.ToList();
    }

    public void Save()
    {
        _db.SaveChanges();
    }
}