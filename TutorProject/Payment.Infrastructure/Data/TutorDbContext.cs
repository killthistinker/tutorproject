using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Infrastructure.Data;

public class TutorDbContext : DbContext
{
    public DbSet<TransactionLog> Logs { get; set; }

    public TutorDbContext(DbContextOptions<TutorDbContext> options) : base(options)
    {
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
    }
}