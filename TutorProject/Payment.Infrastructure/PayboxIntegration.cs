using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using Payment.Domain;
using Payment.Interfaces;
using Responses.DataTransferLib;

namespace Payment.Infrastructure;

public class PayboxIntegration : IPayboxIntegration
{
    private readonly PayboxConfig _payboxConfig;
    private static Random _random = new Random();

    public PayboxIntegration(PayboxConfig payboxConfig)
    {
        _payboxConfig = payboxConfig;
    }
    public async Task<DefaultResponseObject<XmlResponseUrl>> GetUrlPaymentAsync(PaymentDTO log)
    {
        try
        {
            var result = SendRequest(_payboxConfig.GetUrl, new Dictionary<string, object>()
            {
                { "pg_merchant_id", _payboxConfig.MerchantId },
                { "pg_order_id", log.TransactionId },
                { "pg_amount", log.Amount },
                { "pg_currency", "KZT" },
                { "pg_lifetime", 1800.ToString() },
                { "pg_encoding", "UTF-8" },
                { "pg_request_method", "GET" },
                { "pg_description", log.LessonName },
                { "pg_user_contact_email", log.Email },
                { "pg_testing_mode", 0 },
                //{ "pg_success_url", "site.kz/check" }, успешный ответ
                //{ "pg_failure_url", "site.kz/check" }, не успешный ответ 
            });
            
            if (!string.IsNullOrEmpty(result))
            {
                XmlResponseUrl response;
                using (var client = new HttpClient())
                {
                    HttpResponseMessage message = await client.GetAsync(result);
                    var stringResponse = await message.Content.ReadAsStringAsync();
                    response = DeserializeXml<XmlResponseUrl>(stringResponse);
                }

                return response.Status switch
                {
                    "ok" => new DefaultResponseObject<XmlResponseUrl> { Value = response, IsSuccess = true },
                    _ => new DefaultResponseObject<XmlResponseUrl> { Value = response, IsSuccess = false }
                };

            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return new DefaultResponseObject<XmlResponseUrl> { Value = new XmlResponseUrl(){ ErrorDescription = "Ошибка запроса"}, IsSuccess = false };
    }

    public async Task<DefaultResponseObject<XmlResponseStatus>> GetStatusPayment(PaymentStatus data)
    {
        try
        {
            var result = SendRequest(_payboxConfig.GetStatus, new Dictionary<string, object>()
            {
                { "pg_merchant_id", _payboxConfig.MerchantId },
                { "pg_payment_id", data.PayboxId },
                { "pg_order_id", data.OrderId},
            });

            if (!string.IsNullOrEmpty(result))
            {
                XmlResponseStatus response;
                using (var client = new HttpClient())
                {
                    HttpResponseMessage message = await client.GetAsync(result);
                    var stringResponse = await message.Content.ReadAsStringAsync();
                    response = DeserializeXml<XmlResponseStatus>(stringResponse);
                }
                return response.TransactionStatus switch
                {
                    "ok" => new DefaultResponseObject<XmlResponseStatus> { Value = response, IsSuccess = true },
                    _ => new DefaultResponseObject<XmlResponseStatus> { Value = response, IsSuccess = false }
                };

            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return new DefaultResponseObject<XmlResponseStatus> { Value = new XmlResponseStatus(){ Status = "Ошибка запроса"}, IsSuccess = false };
    }

    private string SendRequest(string scriptName, Dictionary<string, object> parameters)
    {
        var request = $"{_payboxConfig.PayboxUrl}{scriptName}?{XmlToUrl(FormRequest(scriptName, parameters))}";
        return request;
    }
    
    private string XmlToUrl(XElement element)
    {
        var result = new StringBuilder();

        foreach (var elem in element.Elements())
        {
            var elements = elem.Elements();

            if (elements.Count() != 0)
            {
                result.Append(XmlToUrl(elem));
            }
            else
            {
                result.Append($"{elem.Name.ToString()}={elem.Value}");
                result.Append("&");
            }
        }

        return result.ToString().TrimEnd('&');
    }
    private XElement FormRequest(string scriptName, Dictionary<string, object> request)
    {
        var xRequest = new XElement("request");
        request.Add("pg_salt", GetRandomSalt());
        AddValues(ref xRequest, request);
        
        StringBuilder inputSb = new StringBuilder();
        if (!string.IsNullOrEmpty(scriptName))
        {
            inputSb.Append(scriptName);
            inputSb.Append(";");
        }

        GetValues(ref inputSb, xRequest);
        inputSb.Append(_payboxConfig.SecretKey);
        var md5 = MD5.Create();
        var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(inputSb.ToString()));

        var sb = new StringBuilder();

        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("x2"));
        }

        xRequest.Add(new XElement("pg_sig", sb.ToString()));

        return xRequest;
    }
    
    private static string GetRandomSalt()
    {
        string generateNumber = DateTime.Now.Ticks.ToString();

        using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
        {
            byte[] bytes = new byte[8];
            rng.GetBytes(bytes);
            generateNumber += Math.Abs(BitConverter.ToInt64(bytes, 0)).ToString();
        }

        var chars = $"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789{generateNumber}";
        return new string(Enumerable.Repeat(chars, 12)
          .Select(s => s[_random.Next(s.Length)]).ToArray());
    }

    private static void GetValues(ref StringBuilder sb, XElement request)
    {
        var elements = request?.Elements();
        if (elements?.Count() != 0)
        {
            foreach (var element in elements)
                GetValues(ref sb, element);
        }
        else
        {
            if (!string.IsNullOrEmpty(request.Value))
            {
                sb.Append(request.Value);
                sb.Append(";");
            }
        }
    }

    private static void AddValues(ref XElement request, Dictionary<string, object> parameters)
    {
        foreach (var param in parameters.OrderBy(x => x.Key))
        {
            if (param.Value is Dictionary<string, object> dictParam)
                AddValues(ref request, dictParam);
            else
                request.Add(new XElement(param.Key, param.Value));
        }
    }

    private dynamic DeserializeXml<T>(string responseString)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        StringReader strReader = new StringReader(responseString);
        T response = (T)serializer.Deserialize(strReader);
        return response;
    }


}