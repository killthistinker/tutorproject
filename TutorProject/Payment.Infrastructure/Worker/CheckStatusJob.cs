﻿using Calabonga.Microservices.BackgroundWorkers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Payment.Infrastructure.Worker
{
    public class CheckStatusJob : CrontabScheduledBackgroundHostedService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        public CheckStatusJob(IServiceScopeFactory serviceScopeFactory, ILogger<CheckStatusJob> logger) : base(serviceScopeFactory, logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override string Schedule => "*/10 * * * *";

        protected override string DisplayName => "PaymentService";

        protected override async Task ProcessInScopeAsync(IServiceProvider serviceProvider, CancellationToken token)
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var service = scope.ServiceProvider.GetRequiredService<CheckBackgroundWorker>();
            await service.CheckStatusPayment();
        }
    }
}
