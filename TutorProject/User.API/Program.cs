using NLog;
using NLog.Web;
using User.Application;
using User.Infrastructure;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
try
{
    var builder = WebApplication.CreateBuilder(args);

    var services = builder.Services;
// Add services to the container.

    services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    services.AddEndpointsApiExplorer();
    services.AddSwaggerGen();
    services.AddInfrastructureServices(builder.Configuration);
    services.AddApplicationServices();
    var app = builder.Build();

// Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();
    app.UseCors("CorsPolicy");
    app.MapControllers();

    app.Run();
}
catch (Exception e)
{
    logger.Error(e, "Остановка программы");
}
finally
{
    NLog.LogManager.Shutdown();
}
