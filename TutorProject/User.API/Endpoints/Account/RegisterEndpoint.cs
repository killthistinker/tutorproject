﻿using Ardalis.ApiEndpoints;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Responses.AccountModels;
using Responses.DataTransferLib;
using User.Infrastructure.Contracts.Services;
using StatusCodes = Responses.Enums.StatusCodes;

namespace User.API.Endpoints.Account;
    [ApiController]
    [Route("[controller]")]
    public class RegisterEndpoint : EndpointBaseAsync.WithRequest<UserRegisterDto>.WithActionResult<
    DefaultResponseObject<DefaultResponse>>
{
    private readonly IAccountService _accountService;
    private readonly IMapper _mapper;
    private readonly ILogger<RegisterEndpoint> _logger;

    public RegisterEndpoint(IAccountService account, IMapper mapper, ILogger<RegisterEndpoint> logger)
    {
        _accountService = account;
        _mapper = mapper;
        _logger = logger;
    }

    [HttpPost("/Account/Register")]
    public override async Task<ActionResult<DefaultResponseObject<DefaultResponse>>> HandleAsync(
        UserRegisterDto request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var response = await _accountService.Register(request);
            if (!response.IsSuccess) return Ok(response);

            return Ok(_mapper.Map<DefaultResponseObject<DefaultResponse>>(response));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Проиошшла ошибка");
            return Ok(StatusCodes.ServiceIsUnavailable);
        }
    }
}