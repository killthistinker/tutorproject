﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Responses.AccountModels;
using User.Infrastructure.Contracts.Services;

namespace User.API.Endpoints
{
    [Route("[controller]")]
    [ApiController]
    public class EditProfleEndpoint : ControllerBase
    {
        private readonly IEditProfileService _editProfileService;
        private readonly UserManager<Domain.Entities.User> _userManager;

        public EditProfleEndpoint(IEditProfileService editProfileuserService, UserManager<Domain.Entities.User> userManager)
        {
            _editProfileService = editProfileuserService;
            _userManager = userManager;
        }
        [HttpPost("/Account/EditProfile")]
        public async Task<ActionResult> EditProfileAsync(EditUserDto request, CancellationToken cancellationToken = new CancellationToken())
        {
            var response = await _editProfileService.EditProfileAsync(request);
            return Ok(response);
        }
    }
}
