﻿using Microsoft.Extensions.Logging;
using Responses.DataTransferLib;
using Responses.Enums;
using Responses.Extensions;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.Infrastructure.Services;

public class RoomService : IRoomService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<UserService> _logger;

    public RoomService(HttpClient httpClient, ILogger<UserService> logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }
    
    public async Task<DefaultResponseObject<StatusCodes>> Add(RoomModelRequest model, string? email)
    {
        try
        {
            model.Email = email;
            var response = await _httpClient.PostAndReturnResponseAsync<RoomModelRequest, StatusCodes>( model, "/Room/Add");
            return response;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Произошла ошибка");
            throw;
        }
    }

    public async Task<DefaultResponseObject<IEnumerable<RoomDto>>> GetAll()
    {
        try
        {
            var response = await _httpClient.GetAndReturnResponseAsync<IEnumerable<RoomDto>>("/Room/All");
            return response;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Произошла ошибка");
            throw;
        }
    }
}