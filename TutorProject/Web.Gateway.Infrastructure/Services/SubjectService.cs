﻿using Microsoft.Extensions.Logging;
using Responses.DataTransferLib;
using Responses.Enums;
using Responses.Extensions;
using Responses.ViewModels;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.Infrastructure.Services;

public class SubjectService : ISubjectService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<UserService> _logger;

    public SubjectService(HttpClient httpClient, ILogger<UserService> logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }

    public async Task<DefaultResponseObject<SubjectsDto>> GetSubjects()
    {
        try
        {
            var response = await _httpClient.GetAndReturnResponseAsync<SubjectsDto>("/Subjects/All");
            return response;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath");
            return new DefaultResponseObject<SubjectsDto>
                { ErrorModel = new ErrorModel { StatusCode = StatusCodes.ServiceIsUnavailable } };
        }
    }
}