﻿using Microsoft.Extensions.Logging;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Responses.Enums;
using Responses.Extensions;
using Web.Gateway.Infrastructure.Helpers;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.Infrastructure.Services;

public class TutorApplicationService : ITutorApplicationService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<UserService> _logger;

    public TutorApplicationService(ILogger<UserService> logger, HttpClient httpClient)
    {
        _logger = logger;
        _httpClient = httpClient;
    }

    public async Task<DefaultResponseObject<StatusCodes>> CreateTutor(TutorAddDto request)
    {
        try
        {
            var response =
                await _httpClient.PostFilesAndReturnResultAsync(request,
                    "/TutorApplications/Create");
            return response;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath.");
            return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceIsUnavailable };
        }
    }
}