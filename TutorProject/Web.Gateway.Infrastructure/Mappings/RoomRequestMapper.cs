﻿using AutoMapper;
using Responses.DataTransferLib;

namespace Web.Gateway.Infrastructure.Mappings;

public class RoomRequestMapper : Profile
{
    public RoomRequestMapper()
    {
        CreateMap<string, RoomModelRequest>()
            .ForMember(dst => dst.Email, opt => opt.MapFrom(src => src));
    }
}
