﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Schedule.Application.Services;
using Schedule.Infrastructure.Contracts.Services;

namespace Schedule.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<IPeriodService, PeriodService>();
        serviceCollection.AddTransient<IPeriodService, PeriodService>();
        serviceCollection.AddAutoMapper(Assembly.GetExecutingAssembly());
        return serviceCollection;
    }
}