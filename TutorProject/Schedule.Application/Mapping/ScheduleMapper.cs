﻿using AutoMapper;
using Responses.DataTransferLib.ScheduleDTO;
using Schedule.Domain.Entities;

namespace Schedule.Application.Mapping;

public class ScheduleMapper : Profile
{
    public ScheduleMapper()
    {
        CreateMap<EmptyPeriodDto, Period>();
        CreateMap<HourDto, Hour>()
            .ForMember(dest => dest.Available, opt => 
                opt.MapFrom(src => src.Available))
            .ForMember(dest => dest.HourName, opt => 
                opt.MapFrom(src => src.HourName))
            .ForMember(dest => dest.Id, opt => 
                opt.Ignore()).ReverseMap();
        
        CreateMap<AvailableHourDto, AvailableHour>();
        CreateMap<AvailableSlotDto, AvailableSlot>();
    }
}