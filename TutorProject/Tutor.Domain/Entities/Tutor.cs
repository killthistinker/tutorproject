﻿namespace Tutor.Domain.Entities;

public class Tutor
{
    public long Id { get; set; }
    public string Email { get; set; }
    public List<Specialise> Specialises { get; set; }

    public Tutor()
    {
        Specialises = new List<Specialise>();
    }
}