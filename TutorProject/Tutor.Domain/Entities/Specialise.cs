﻿namespace Tutor.Domain.Entities;

public class Specialise
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string SubjectName { get; set; }
}