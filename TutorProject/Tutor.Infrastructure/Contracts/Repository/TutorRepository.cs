﻿using Tutor.Infrastructure.Contracts.Repository.Interfaces;
using Tutor.Infrastructure.Data.TutorDbContext;

namespace Tutor.Infrastructure.Contracts.Repository;

public class TutorRepository : ITutorRepository
{
    private readonly TutorDbContext _context;

    public TutorRepository(TutorDbContext context)
    {
        _context = context;
    }

    public IEnumerable<Domain.Entities.Tutor>? GetAll() => _context.Tutors;

    public async Task CreateAsync(Domain.Entities.Tutor item) => await _context.Tutors.AddAsync(item);
    public void Create(Domain.Entities.Tutor item) => _context.Tutors.Add(item);
    public void Update(Domain.Entities.Tutor item) => _context.Tutors.Update(item);
    public void Remove(Domain.Entities.Tutor item) => _context.Tutors.Remove(item);
    public void Save() => _context.SaveChanges();
    public async Task SaveAsync() => await _context.SaveChangesAsync();
}