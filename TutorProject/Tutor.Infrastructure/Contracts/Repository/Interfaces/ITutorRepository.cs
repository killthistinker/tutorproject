﻿using Responses.Contracts.Repository;

namespace Tutor.Infrastructure.Contracts.Repository.Interfaces;

public interface ITutorRepository : IRepository<Domain.Entities.Tutor>
{
    
}