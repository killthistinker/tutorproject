﻿using Microsoft.EntityFrameworkCore;


namespace Tutor.Infrastructure.Data.TutorDbContext;

public class TutorDbContext : DbContext
{
    public DbSet<Domain.Entities.Tutor> Tutors { get; set; }
    
    public TutorDbContext(DbContextOptions<TutorDbContext> options) : base(options)
    {
        
    }
}