using Payment.Domain;
using Responses.DataTransferLib;

namespace Payment.Interfaces;

public interface IPayboxIntegration
{
    //
    Task<DefaultResponseObject<XmlResponseUrl>> GetUrlPaymentAsync(PaymentDTO log);
    Task<DefaultResponseObject<XmlResponseStatus>> GetStatusPayment(PaymentStatus data);
}