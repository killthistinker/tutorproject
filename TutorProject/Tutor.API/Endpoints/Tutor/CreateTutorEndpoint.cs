﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Tutor.Application.Services.Interfaces;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Tutor.Api.Endpoints.Tutor;

public class CreateTutorEndpoint : EndpointBaseAsync.WithRequest<TutorCreateDto>.WithActionResult<DefaultResponseObject<StatusCodes>>
{
    private readonly ITutorService _tutorService;
    private readonly IEmailSendService _emailSendService;

    public CreateTutorEndpoint(ITutorService tutorService, IEmailSendService emailSendService)
    {
        _tutorService = tutorService;
        _emailSendService = emailSendService;
    }

    [HttpPost("/Tutors/Create")]
    public async override Task<ActionResult<DefaultResponseObject<StatusCodes>>> HandleAsync(TutorCreateDto request, CancellationToken cancellationToken = new CancellationToken())
    {
        var response = await _tutorService.CreateTutor(request);
        return Ok(response);
    }
}