﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Tutor.Application.Services.Interfaces;
using Tutor.Domain.Models.Dto;

namespace Tutor.Api.Endpoints.Tutor;

public class GetAllTutorEndpoint : EndpointBaseSync.WithoutRequest.WithActionResult<DefaultResponseObject<IEnumerable<TutorDto>>>
{
    private readonly ITutorService _tutorService;

    public GetAllTutorEndpoint(ITutorService tutorService)
    {
        _tutorService = tutorService;
    }
    
    [HttpGet("/Tutors/All")]
    public override ActionResult<DefaultResponseObject<IEnumerable<TutorDto>>> Handle()
    {
        var tutors = _tutorService.GetAll();

        return Ok(tutors);
    }
}