﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Tutor.Application.Services.Interfaces;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Tutor.Api.Endpoints.Tutor;

public class RefuseTutorEndpoint : EndpointBaseAsync.WithRequest<RefuseTutorDto>.WithActionResult<DefaultResponseObject<StatusCodes>>
{
    private readonly ITutorService _tutorService;

    public RefuseTutorEndpoint(ITutorService tutorService)
    {
        _tutorService = tutorService;
    }

    [HttpPost("/Tutors/Refuse")]

    public override async Task<ActionResult<DefaultResponseObject<StatusCodes>>> HandleAsync(RefuseTutorDto request, CancellationToken cancellationToken = new CancellationToken())
    {
        var response = await _tutorService.ResponseTutor(request);
        return Ok(response);
    }
}