﻿using Responses.AccountModels;
using Responses.DataTransferLib;
using Responses.ViewModels;

namespace Web.Gateway.Interfaces.Contracts;

public interface ISubjectService
{
    public Task<DefaultResponseObject<SubjectsDto>> GetSubjects();
}