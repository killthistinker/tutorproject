﻿using Responses.DataTransferLib;
using Responses.Enums;

namespace Web.Gateway.Interfaces.Contracts;

public interface IRoomService
{
    public Task<DefaultResponseObject<StatusCodes>> Add(RoomModelRequest model, string? email);
    public Task<DefaultResponseObject<IEnumerable<RoomDto>>> GetAll();
}