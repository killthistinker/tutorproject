﻿using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Responses.Enums;

namespace Web.Gateway.Interfaces.Contracts;

public interface ITutorApplicationService
{
    Task<DefaultResponseObject<StatusCodes>> CreateTutor(TutorAddDto request);
}