﻿using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Responses.Enums;

namespace Web.Gateway.Interfaces.Contracts;

public interface ITutorService
{
    public Task<DefaultResponseObject<StatusCodes>> AddTutor(TutorCreateDto model);
    public Task<DefaultResponseObject<StatusCodes>> RefuseTutor(RefuseTutorDto model);
}