using Responses.DataTransferLib;
using Responses.Enums;
using Responses.Payments;

namespace Web.Gateway.Interfaces.Contracts;

public interface IPaymentService
{
    public Task<DefaultResponseObject<PayResponse>> GetPagePaybox(PaymentCourseDto model);
    public Task<DefaultResponseObject<List<Transaction>>> GetListPayments(GetPaymentDto email);
}