import api from '../requests'

export const register = async (payload) =>
  await api.post('/Account/Register', payload).catch(error => { console.log(error) })

export const authenticate = async (payload) =>
  await api.post('/Account/Login', payload)

export const getUserData = async () => await api.get('/Account/GetUserData')

export const logOut = async () => await api.post('/Account/LogOut')
