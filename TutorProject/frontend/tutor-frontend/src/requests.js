import axios from 'axios'

const api = axios.create({
  baseURL: 'https://localhost:7063'
})

api.defaults.withCredentials = 'include'
api.defaults.headers.common['Content-Type'] = 'application/json'

function errorHandler (error) {
  if (error.response?.status === 401) {
    window.location = '/'
  }

  return Promise.reject(error)
}

api.interceptors.request.use((config) => {
  return config
}, errorHandler)

api.interceptors.response.use((response) => {
  return response
}, errorHandler)

export default api
