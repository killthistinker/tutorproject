﻿using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Schedule.Domain.Entities;

public class Hour
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    [JsonPropertyName("id")]
    public string Id { get; set; }
    
    [JsonPropertyName("hourName")]
    public string HourName { get; set; }
    
    [JsonPropertyName("available")]
    public bool Available { get; set; } = false;
}