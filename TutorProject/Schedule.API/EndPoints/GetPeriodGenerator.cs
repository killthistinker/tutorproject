﻿using System.Collections.Specialized;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.DataTransferLib.ScheduleDTO;
using Schedule.Infrastructure.Contracts.Services;

namespace Schedule.API.EndPoints;

[ApiController]
[Route("[controller]")]

public class GetPeriodGenerator : EndpointBaseSync.WithoutRequest.WithActionResult<DefaultResponseObject<EmptyPeriodDto>>
{
    private readonly IPeriodService _periodService;

    public GetPeriodGenerator(IPeriodService periodService)
    {
        _periodService = periodService;
    }
    
    [HttpGet("GetEmptyPeriod")]
    public override ActionResult<DefaultResponseObject<EmptyPeriodDto>> Handle()
    {
        var emptyPeriod = _periodService.GetEmptyPeriod();
    
        return Ok(emptyPeriod);
    }
}