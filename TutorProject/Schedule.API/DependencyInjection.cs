﻿namespace Schedule.API;

public static class DependencyInjection
{
    public static IServiceCollection SetCorsPolicy(this IServiceCollection services)
    {
        services.AddCors(options => options.AddPolicy("CorsPolicy", policy =>
        {
            policy.WithOrigins("https://localhost:7063").AllowAnyMethod().AllowAnyHeader().AllowCredentials();
        }));
        return services;
    }
}