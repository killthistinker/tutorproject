using System.ComponentModel.DataAnnotations;

namespace Responses.Payments;

public class PaymentCourseDto
{
    public DateTime LessonDate { get; set; }
    public string LessonName { get; set; }
    [Range(10, 99999, ErrorMessage = "Пожалуйста, Введите значение, большее {10}")]
    public int Amount { get; set; }
    public string? Email { get; set; }
}