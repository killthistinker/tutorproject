﻿namespace Responses.DataTransferLib;

public class RoomModelRequest
{
    public string? Name { get; set; }
    public string? Email { get; set; }
}