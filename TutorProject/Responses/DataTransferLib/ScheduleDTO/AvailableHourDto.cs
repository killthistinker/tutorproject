﻿namespace Responses.DataTransferLib.ScheduleDTO;

public class AvailableHourDto
{
    public int Month { get; set; }
    
    public int Day { get; set; }
    
    public List<HourDto>? Hours { get; set; }
}