﻿namespace Responses.DataTransferLib.ScheduleDTO;

public class EmptyPeriodDto
{
    public string Email { get; set; }
    public AvailableSlotDto AvailebleSlots { get; set; }
}