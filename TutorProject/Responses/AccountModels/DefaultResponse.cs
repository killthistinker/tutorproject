namespace Responses.AccountModels;

public class DefaultResponse
{
    public bool IsResult { get; set; }
    public string? ErrorDescription { get; set; }
}