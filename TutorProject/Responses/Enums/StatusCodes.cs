﻿namespace Responses.Enums;

public enum StatusCodes
{
    ServiceIsUnavailable = 601,
    UserNotFound = 602,
    RoomsNotFound = 603,
    UserIsNotAuthorized = 604,
    UserExists = 605,
    ServiceOk = 700
}