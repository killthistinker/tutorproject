namespace Payment.Domain.Entities;

public class TransactionLog
{
    public int Id { get; set; }
    public string Email { get; set; }
    public DateTime LessonDate { get; set; }
    public int Amount { get; set; }
    public string LessonName { get; set; }
    public string Status { get; set; }
    public DateTime CreateDate { get; set; }
    public int PayboxId { get; set; }
    public string? Card { get; set; }
    public int? CheckCount { get; set; }

}