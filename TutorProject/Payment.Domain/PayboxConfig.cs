namespace Payment.Domain;

public class PayboxConfig
{
    public string SecretKey { get; set; }
    public int MerchantId { get; set; }
    public string PayboxUrl { get; set; }
    public string GetUrl { get; set; }
    public string GetStatus { get; set; }
}