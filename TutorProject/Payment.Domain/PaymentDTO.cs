namespace Payment.Domain;

public class PaymentDTO
{
    public DateTime LessonDate { get; set; }
    public string LessonName { get; set; }
    public int Amount { get; set; }
    public string? Email { get; set; }
    public string? TransactionId { get; set; }
}

