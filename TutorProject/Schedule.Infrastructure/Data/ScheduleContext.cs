﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Schedule.Domain.Entities;

namespace Schedule.Infrastructure.Data;

public class ScheduleContext : IScheduleContext
{
    public ScheduleContext(IOptions<DBConfig> options, IConfiguration configuration)
    {
        MongoClient client = new MongoClient(options.Value.ConnectionString);
        IMongoDatabase dataBase = client.GetDatabase(options.Value.DatabaseName);
        Periods = dataBase.GetCollection<Period>(options.Value.PeriodCollection);
        Hours = dataBase.GetCollection<Hour>(options.Value.HourCollection);
        ScheduleContextSeed.SeedData(Hours);
    }

    public IMongoCollection<Period> Periods { get; }
    public IMongoCollection<Hour> Hours { get; }
}