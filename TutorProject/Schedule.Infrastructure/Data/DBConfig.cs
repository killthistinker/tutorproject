﻿namespace Schedule.Infrastructure.Data;

public class DBConfig
{
    public string ConnectionString { get; set; } = "mongodb://localhost:27017/TutorSchedules";
    public string DatabaseName { get; set; } = "TutorSchedules";
    public string PeriodCollection { get; set; } = "Period";
    public string HourCollection { get; set; } = "Hour";
}