﻿using MongoDB.Driver;
using Schedule.Domain.Entities;
using Schedule.Infrastructure.Contracts.Repository.Interfaces;
using Schedule.Infrastructure.Data;

namespace Schedule.Infrastructure.Contracts.Repository;

public class PeriodRepository : IPeriodRepository
{
    private readonly IScheduleContext _context;
    
    public PeriodRepository(IScheduleContext context)
    {
        _context = context;
    }

    public void Create(Period period) => _context.Periods.InsertOne(period);

    public List<Hour> GetTime()
    {
        var hours = _context.Hours.Find(p => true).ToList();
        return hours;
    }
}